<?php

use Illuminate\Database\Seeder;
use App\appoinmentTable;
use App\patientTable;
use App\doctorTable;
//use DB;
class AppointmentTableSeeder extends Seeder
{

   public function run()
    {

    	// you have to disable the foreign key constrain before 
    	// applyin gthe migration
    	Schema::disableForeignKeyConstraints();
    	$faker = \Faker\Factory::create();

		// first take all the id from the patient table and make an array
    	$patients = patientTable::all()->pluck('id')->toArray();

		// second take all the id from the doctor table and make an array
        $doctors = doctorTable::all()->pluck('id')->toArray();

        DB::table('appoinment_tables')->delete();
        for($i=0;$i<=100;$i++){

        	// take a random doctor and patient from the list
        	$patient_id = 	$faker->randomElement($patients);
 			$doctor_id = 	$faker->randomElement($doctors);
        	DB::table('appoinment_tables')->insert([
                "App_reason" => Str::random(60),
                "scheduled_for" => $patient_id,
                "scheduled" => $doctor_id,
                "App_reason" => Str::random(60),
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ]);

        }

}    
}