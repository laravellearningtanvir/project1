<?php

use Illuminate\Database\Seeder;
use App\doctorTable;
class DoctorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        // add the faker for creating the seeder
        // Truncate the table 
        //doctorTable::truncate();
        
        $faker = \Faker\Factory::create();

        // loop through the 
        for($i=0;$i<=100;$i++){
            // create the instance
            doctorTable::create([
                "Doc_mobile" => $faker->unique()->randomNumber,
                "Doc_firstname" => $faker->firstName,
                "Doc_lastname" => $faker->lastName,
                "Doc_dept" => "ENT",
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ]);
        }
    }
}