<?php

use Illuminate\Database\Seeder;
use App\paymentTable;
use App\appoinmentTable;

class BillTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {

    	// to create a relationship perfectly we 
    	// need the key of the other related data
    	// so we can place it insed of random  number so it can  be a perfect seeder
    	// you only fetch the id array if you need anything 
    	// you dont need it if you provide anything


    	// you have to disable the foreign key constrain before 
    	// applyin gthe migration

  

    	Schema::disableForeignKeyConstraints();
    	$faker = \Faker\Factory::create();

    	// take all the data id the appointment table
    	$appointments = appoinmentTable::all()->pluck('id')->toArray();

		// take all the id from the payment
        //$payments = paymentTable::all()->pluck('id')->toArray();

        DB::table('bill_tables')->delete();
        for($i=0;$i<=100;$i++){

        	// take a random doctor and patient from the list
        	$appointment_id = 	$faker->randomElement($appointments);
 			//$payment_id = 	$faker->randomElement($payments);
        	DB::table('bill_tables')->insert([
                "Bill_number" => rand(10,100),
                "Bill_status" => 1,
                //"Pay_amount" => 500,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
                "generates" => $appointment_id
            ]);

        }
    }
}

